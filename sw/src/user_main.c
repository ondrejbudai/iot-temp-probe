#include "osapi.h"
#include "user_interface.h"
#include "driver/uart.h"

#include "espconn.h"

#include "ds18b20.h"

DS18B20_Sensors sensors;

static os_timer_t ptimer;

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABBBCDDD
 *                A : rf cal
 *                B : at parameters
 *                C : rf init data
 *                D : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        case FLASH_SIZE_64M_MAP_1024_1024:
            rf_cal_sec = 2048 - 5;
            break;
        case FLASH_SIZE_128M_MAP_1024_1024:
            rf_cal_sec = 4096 - 5;
            break;
        default:
            rf_cal_sec = 0;
            break;
    }
    return rf_cal_sec;
}

void blinky(void *arg)
{
	static uint8_t state = 0;

	if (state) {
		GPIO_OUTPUT_SET(5, 1);
	} else {
		GPIO_OUTPUT_SET(5, 0);
	}
	state ^= 1;
}
void ICACHE_FLASH_ATTR user_pre_init(void){}

static ETSTimer udpReadyTimer;
static ETSTimer sendDataTimer;
static struct espconn config = {};
static esp_udp udpConfig = {};

void ICACHE_FLASH_ATTR udpReadyCb(void* data){
    os_printf("UDP READY CALLBACK\n");
    const char* testMsg = "test 42 -1\n";

    espconn_sendto(&config, testMsg, strlen(testMsg));
    os_printf("UDP READY CALLBACK SENDED\n");
}

void ICACHE_FLASH_ATTR sendData(void* arg){
    os_printf("Pre-creating UDP...\n");
    udpConfig.remote_port = 2003;
    const static uint8 udp_server_ip[4] = {104, 248, 142, 185};
    memcpy(udpConfig.remote_ip, udp_server_ip, sizeof(udp_server_ip));

    config.type = ESPCONN_UDP;
    config.proto.udp = &udpConfig;
    os_printf("Creating UDP...\n");
    sint8 result = espconn_create(&config);
    if(result){
        os_printf("UDP create failed: %d\n", result);
    }
    os_printf("UDP created\n");

    // os_timer_disarm(&udpReadyTimer);
    // os_timer_setfn(&udpReadyTimer, (os_timer_func_t *)udpReadyCb, NULL);
    // os_timer_arm(&udpReadyTimer, 1000, 0);


    os_printf("UDP READY CALLBACK\n");

    float temp = ds18b20_read(&sensors, 0);

    

    char msg[40];

    os_sprintf(msg, "test2 %d -1\n", (int)(temp * 1000));
    
    os_printf(msg);

    espconn_sendto(&config, msg, strlen(msg));
    os_printf("UDP READY CALLBACK SENDED\n");

    os_printf("sensors found: %d\n", sensors.length);

    ds18b20_request_temperatures(&sensors);

    // config.proto.udp.remote_ip;
}

static ETSTimer wifiLinker;

void ICACHE_FLASH_ATTR wifiCheckIp(void* arg) {
    os_timer_disarm(&wifiLinker);

    struct ip_info ipConfig;

    wifi_get_ip_info(STATION_IF, &ipConfig);
    uint8_t status = wifi_station_get_connect_status();

    os_printf("status: %d\n", status);
    os_printf("wifiCheckIp\n");

    switch(status){
        case STATION_GOT_IP:
            os_printf("STATION_GOT_IP\n");
            // sendData();
        break;
        case STATION_WRONG_PASSWORD:
            os_printf("STATION_WRONG_PASSWORD\n");
        break;
        case STATION_NO_AP_FOUND:
            os_printf("STATION_NO_AP_FOUND\n");
        break;
        case STATION_CONNECT_FAIL:
            os_printf("STATION_CONNECT_FAIL\n");
        break;
        case STATION_IDLE:
            os_printf("STATION_IDLE\n");
            os_timer_arm(&wifiLinker, 1000, 0);
        break;
        case STATION_CONNECTING:
            os_printf("STATION_CONNECTING\n");
            os_timer_arm(&wifiLinker, 1000, 0);
        break;
    }
}


void ICACHE_FLASH_ATTR user_init(void)
{
    gpio_init();

    wifi_set_opmode_current(STATION_MODE);
    
    struct station_config config = {};

    strcpy(config.ssid, "SM-NET");
    strcpy(config.password, "mojetajneheslo");

    struct ip_info staticIpInfo = {};
    IP4_ADDR(&staticIpInfo.ip, 192, 168, 1, 253);
    IP4_ADDR(&staticIpInfo.gw, 192, 168, 1, 1);
    IP4_ADDR(&staticIpInfo.netmask, 255, 255, 255, 0);
    wifi_station_dhcpc_stop();
    wifi_set_ip_info(STATION_IF, &staticIpInfo);
    wifi_station_set_config_current(&config);

    os_timer_disarm(&wifiLinker);
    os_timer_setfn(&wifiLinker, wifiCheckIp, NULL);
    os_timer_arm(&wifiLinker, 1000, 0);


    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);

    uart_div_modify(0, UART_CLK_FREQ / 115200);

    ds18b20_init(&sensors);
    ds18b20_get_all(&sensors);
    // ds18b20_set_resolution(&sensors, 0, DS18B20_TEMP_12_BIT);
    ds18b20_request_temperatures(&sensors);

    

    os_timer_disarm(&ptimer);
    os_timer_setfn(&ptimer, (os_timer_func_t *)blinky, NULL);
    os_timer_arm(&ptimer, 500, 1);

    os_timer_disarm(&sendDataTimer);
    os_timer_setfn(&sendDataTimer, (os_timer_func_t *)sendData, NULL);
    os_timer_arm(&sendDataTimer, 1000, 1);
}