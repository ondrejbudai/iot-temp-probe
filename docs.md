# IoT teplotní sonda
Ondřej Budai <475970@mail.muni.cz>

## Cíle
Cílem projektu bylo vytvořit IoT sondu pro měření teploty. Sonda by měla být napájena z baterií a periodicky vysílat naměřená data do sběrného místa pro další zpracování. Cílová perioda je 10 minut, tedy hodnota která plně dostačuje pro zachycení běžných změn teplot.

## Realizace
### Hardware
#### Procesor
Jako základ teploměru jsem zvolil modul ESP-WROOM-02 od společnosti Espressif, který je postaven na mikroprocesoru ESP8266 a 1 MB FLASH paměti pro uživatelský program. Pro tuto aplikaci je procesor ESP8266 vhodný tím, že obsahuje Wi-Fi modul včetně kompletní podpory TCP/IP. Díky těmto vlastnostem použitého procesoru může být finální kód, který odesílá teploty do sběrného místa, velmi jednoduchý. Zároveň díky širokému rozšíření Wi-Fi sítí je možné nasadit sondu prakticky kdekoliv. 

Nevýhodou řešení pomocí Wi-fi jsou velké energetické nároky. Konkrétně čip ESP má příkon 50 mW s vypnutým modem, se zapnutým modem je to však již 330 mW, pokud čip v danou chvíli vysílá. Tento problém je možné částečně eliminovat tím, že mezi měřeními bude modem vypnut, čímž se průměrná spotřeba dramaticky sníží. Bohužel navázání připojení přes Wi-Fi síť trvá poměrně dlouho dobu v řádu několika sekund. Tento čas je možné částečně optimalizovat dvěma způsoby: Prvním způsobem je použití slabšího zabezpečení WEP oproti dnes standardní WPA2. Vzhledem k tomu, že ESP není příliš výkonný procesor, tato změna má velký vliv na dobu připojení. Nicméně je potřeba brát v potaz, že WEP je velmi snadno napadnutelné zabezpečení, a proto tato optimalizace není příliš vhodná. Druhou možností optimalizace je použití statické IP místo výchozí automatické konfigurace pomocí DHCP. Tato optimalizace nepřináší žádné bezpečnostní nevýhody, nicméně v případě více sond připojených na jednu síť je třeba zabezpečit nastavení rozdílných IP adres.

#### Napájení
Modul ESP-WROOM-02 vyžaduje pro svoji funkci napětí 3,3 V, proto je potřeba napětí z baterií upravit. Pro tento účel jsem vybral buck boost měnič TPS63031 od společnosti Texas Instruments, který ze vstupní napětí 1,8 - 5,5 V vyrábí napětí 3,3 V při maximální proudu 500mA a účinnosti až 96%. Vzhledem k širokému rozsahu vstupního napětí je tedy sondu možné napájet z dvou nebo tří tužkových baterií, jednoho Li-Pol článku, případně USB powerbanky (zde je ovšem potřeba počítat s nižší účinností vzhledem k tomu, že v power bance je další měnič).

#### Měření teploty
Pro měření teploty je možné na desku osadit 2 typy čidel: Prvním je modul DHT-22, který má tu výhodu, že dokáže měřit i vlhkost vzduchu. Druhým čidlem je DS18B20 od firmy Dallas, které komunikuje pomocí sběrnice OneWire. Každé vyrobené čidlo tohoto typu má přiřazené unikátní identifikační číslo. Výhoda tohoto čidla je tedy ta, že je možné na jeden pin procesoru připojit až 50 těchto čidel. Vzhledem k tomu, že je toto čidlo na trhu dostupné i ve vodotěsné variantě s 2metrovým kabelem, je možné jednou sondou měřit teplotu na velkém množství míst.

### Software
Software je rozdělen na dvě části - první část běží na samotné sondě, druhá část, tedy kolektor, pak běží na linuxovém serveru.

#### Sonda
Vzhledem k tomu, že základní knihovna k ESP8266 obsahuje již všechny důležité komponenty pro práci s TCP/IP, je implementovaná funkcionalita velmi jednoduchá. Mikroprocesor sondy je většinu čas uspaný v režimu deep-sleep s nízkou spotřebou a každých 10 minut se probouzí. Během fáze probuzení nejprve iniciuje Wi-Fi připojení a následně odešle teploměrům pokyn k tomu, aby začaly měřit teplotu. Po připojení a přečtení hodnot teplot z teploměrů se odešle krátká UDP zpráva s naměřenými daty do kolektoru. Výhodou UDP je, že se nemusí čekat na potvrzení, a proto je možné velmi rychle procesor opět uspat. Nevýhodou je, že přenos dat není garantovaný. V testovacích podmínkách však s výpadky paketů nebyl zaznamenaný problém. V případě potíží by bylo možné triviální úpravou kódu změnit protokol na TCP s garantovaným doručením dat.

Ke komunikaci s teplotními čidly jsou použity knihovny dostupné pod svobodnou licencí na serveru GitHub.

#### Kolektor
Kolektor dat běží na operačním systému Linux na virtuálním stroji. Zde jsou umístěné 2 Docker kontejnery, první obsahuje kolektor dat a databázi Graphite, druhý vizualizační software Grafana.

**Graphite** zajišťuje příjem dat na UDP, případně TCP vrstvě, ukládá je do časové databáze a poskytuje API na práci s nimi. Umožňuje detailní nastavení časových retencí: Například je možné nastavit ukládání dat v intervalu 10 minut po dobu 1 měsíce a 1 hodiny po dobu 1 roku. V tomto případě pak Graphite automaticky průměruje data starší než 1 měsíc do delšího intervalu.

**Grafana** je vizualizační software, který má podporu pro čtení dat z databáze Graphite a následně umí tato data vykreslit do velkého množství grafů, tabulek a podobně. Umožňuje také definovat upozornění, která se odesílají e-mailem na definované adresy v případě, že nějaká veličina dosáhla definované hodnoty.

![](projekt.png)