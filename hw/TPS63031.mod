PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
10SON
$EndINDEX
$MODULE 10SON
Po 0 0 0 15 00000000 00000000 ~~
Li 10SON
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 1.79766 0.381328 0.480658 0.480658 900 0.05 N V 21 "10SON"
T1 0 0 1 0.9 0 0.05 N H 21 "VAL**"
DS -1.3 1.3 1.3 1.3 0.08 21
DS 1.3 1.3 1.3 -1.3 0.08 21
DS 1.3 -1.3 -1.3 -1.3 0.08 21
DS -1.3 -1.3 -1.3 1.3 0.08 21
DC -1.5 1.9 -1.21716 1.9 0 21
DP 0 0 0 0 4 0.127 19
Dl -0.8 0.4
Dl -0.4 0.4
Dl -0.4 -0.4
Dl -0.8 -0.4
DP 0 0 0 0 4 0.127 19
Dl 0.4 0.4
Dl 0.8 0.4
Dl 0.8 -0.4
Dl 0.4 -0.4
$PAD
Sh "EXP" R 0.95 2 0 0 900
At SMD N 00888000
.SolderMask 0
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "3" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 1.25
$EndPAD
$PAD
Sh "8" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 -1.25
$EndPAD
$PAD
Sh "7" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.5 -1.25
$EndPAD
$PAD
Sh "9" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.5 -1.25
$EndPAD
$PAD
Sh "6" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1 -1.25
$EndPAD
$PAD
Sh "10" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1 -1.25
$EndPAD
$PAD
Sh "1" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1 1.25
$EndPAD
$PAD
Sh "2" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.5 1.25
$EndPAD
$PAD
Sh "4" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.5 1.25
$EndPAD
$PAD
Sh "5" R 0.8 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1 1.25
$EndPAD
$EndMODULE 10SON
